﻿using System;
using System.Linq;

namespace CodingForExamination
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Fibonacci de 16:\t\t"+Fibonacci(16));
            Console.WriteLine("Fibonacci récursive de 16:\t" + FibonacciRecur(16));
            Console.WriteLine("Factorielle de 7:\t\t"+Factorielle(7));
            Console.WriteLine("Factorielle récursive de 7:\t"+FactorielleRecur(7));
            Console.WriteLine("Tas de lettre de Banana");
            LetterBucket();
            string[] testRunLength = new string[] { "AAA", "ABC", "CCAA", "BBBBBCCCCAAAA", "ABCCBBA", "AAAAAAAABBBBCC" };
            float[] point1 = new float[] { 2.3f, 5.1f };
            float[] point2 = new float[] { 3f, 1.1f };
            Console.WriteLine("Length Encoding");
            for (int i = 0; i < testRunLength.Length; i++)
            {
                Console.WriteLine("\t"+testRunLength[i] + "-------------------->" + RunLengthEncoding(testRunLength[i]));
            }
            Console.WriteLine("Distance euclidienne :\t\t" + EuclidianDistance(point1, point2));
            Console.WriteLine("Somme des pairs dans 10:\t"+SumOfOddInANumber(10));
            Console.ReadLine();
        }
        public static int Fibonacci(int index)
        {
            if (index == 1 || index == 2)
            {
                return 1;
            }
            int[] fibonaciSuite = new int[index];
            fibonaciSuite[0] = 1;
            fibonaciSuite[1] = 1;

            for (int i = 2; i < index; i++)
            {
                fibonaciSuite[i] = fibonaciSuite[i - 1] + fibonaciSuite[i - 2];
            }


            return fibonaciSuite[index - 1];

        }
        public static int FibonacciRecur(int index)
        {
            if (index == 1 || index == 2)
            {
                return 1;
            }

            return FibonacciRecur(index - 1) + FibonacciRecur(index - 2);
        }
        public static int Factorielle(int num)
        {

            int numResult = 1;
            if (num==1)
            {
                return 1;
            }
            for (int i = num; i >= 1; i--)
            {
                numResult *= i ;
            }
            return numResult;
        }
        public static int FactorielleRecur(int num)
        {
            if (num == 1)
            {
                return 1;
            }

            return  FactorielleRecur(num-1)*num ;
        }
        public static void LetterBucket(string word="Banana")
        {
            string inputString = word;
            while (inputString.Length!=0)
            {
                char letter = char.Parse(inputString.Substring(0, 1));
                int countLetter = inputString.Count(obj => obj == letter);
                Console.WriteLine(string.Concat(Enumerable.Repeat(letter, countLetter)));
                inputString = inputString.Replace(letter.ToString(), String.Empty);
            }
            
        }
        public static string RunLengthEncoding(String str, bool isDebug = false)
        {
            string output = "";
            int letterCount = 1;
            bool isOtherLetter = false;
            for (int i = 0; i < str.Length; i++)
            {
                if (i != str.Length - 1)
                {
                    isOtherLetter = str[i] != str[i + 1];
                }
                if (isDebug) Console.WriteLine(isOtherLetter + "    " + letterCount + "  " + str[i]);
                if (i == str.Length - 1 || isOtherLetter)
                {
                    if (letterCount == 1)
                    {
                        output += str[i];
                    }
                    else
                    {
                        output += letterCount.ToString() + str[i];
                    }
                    letterCount = 1;
                }
                else
                {
                    letterCount++;
                }
            }
            return output;
        }
        public static Double EuclidianDistance(float[] point1, float[] point2)
        {
            if (point1.Length != point2.Length)
            {
                return 0.0f;
            }
            Double euclidianDistance = 0.0f;
            for (int i = 0; i < point1.Length; i++)
            {
                euclidianDistance += Math.Pow(point1[i] - point2[i], 2);
                if (i == point1.Length - 1)
                {
                    euclidianDistance = Math.Sqrt(euclidianDistance);
                }

            }
            return euclidianDistance;
        }

        public static int SumOfOddInANumber(int number, int result = 0)
        {
            if (number == 0)
            {
                return result;
            }
            if (number % 2 == 0)
            {
                result += number;
            }
            return SumOfOddInANumber(number - 1, result);
        }

    }
    
}
