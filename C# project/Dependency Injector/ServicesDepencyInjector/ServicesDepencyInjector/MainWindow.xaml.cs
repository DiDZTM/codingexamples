﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MyDependencyLib;

namespace ServicesDepencyInjector
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public DependencyInjector dependencyInjector;
        private static string  uriAddButton;
        private List<String> availableServiceList;

        public event PropertyChangedEventHandler PropertyChanged;
        
        public List<string> AvailableServiceList
        {
            get { return availableServiceList; }
            set { availableServiceList = value; RaiseProperChanged(); }
        }
        public static string UriAddButton
        {
            get
            {
                return uriAddButton;
            }
            set
            {
                uriAddButton = value;
            }
        }
      
        public MainWindow()
        {
            InitializeComponent();
            dependencyInjector = DependencyInjector.Instance;
            string path = Environment.CurrentDirectory;
            path = path.Remove(path.LastIndexOf('\\'));
            path = path.Remove(path.LastIndexOf('\\'))+"\\Images\\add.png";
            UriAddButton = path;
            Console.WriteLine(UriAddButton);
            AvailableServiceList = dependencyInjector.AvailableServiceList;
            
           // availableServiceListView.ItemsSource = dependencyInjector.AvailableServiceList;
            //activeServiceListView.ItemsSource = dependencyInjector.ActiveServiceList;
           this.DataContext = this;
            
        }

        private void AddServiceButton_TouchDown(object sender, TouchEventArgs e)
        {
         
        }
        private void RaiseProperChanged([CallerMemberName] string caller = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(caller));
            }
        }
    }
}
