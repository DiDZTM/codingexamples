﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using MySql.Data.MySqlClient;
namespace MyDependencyLib
{
   /* class SetUp
    {
        public static void Main(string[] args)
        {
            DependencyInjector dependencyInjector;
            dependencyInjector = DependencyInjector.Instance;

            MySQLService dbService = (MySQLService)dependencyInjector.LoadService<MySQLService>();
            dependencyInjector.SearchService<MySQLService>();
            dependencyInjector.UnLoadService<MySQLService>();
            dependencyInjector.LoadService<MySQLService>();
            Console.ReadLine();
        }
    }*/
    
    public sealed class DependencyInjector
    {

        private static DependencyInjector instance = null;
        private List<IService> activeServiceList;
        private List<string> availableServiceList;
        private Type serviceType = typeof(IService);

        public List<IService> ActiveServiceList {
            get { return activeServiceList; }
            
        }
        public List<string> AvailableServiceList
        {
            get { return availableServiceList; }

        }
        public static DependencyInjector Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DependencyInjector();
                    instance.activeServiceList = new List<IService>();
                    instance.availableServiceList = new List<string>();
                    //Here add all new created services
                    Assembly assembly = Assembly.GetExecutingAssembly();
                    Type[] types = assembly.GetTypes();
                    IEnumerable<Type> imp = types.Where(t => t.GetInterfaces().Contains(instance.serviceType));
                    foreach (Type type in imp)
                    {
                        instance.availableServiceList.Add(type.Name);
                    }
                }
                return instance;
            }

        }

        public IService SearchService<T>() where T : IService
        {
            List<T> test = activeServiceList.OfType<T>().ToList();
            if (test.Count > 0)
            {
                return test[0];
            }
            return null;
        }
        public IService LoadService<T>() where T : IService, new()
        {
            List<T> test = activeServiceList.OfType<T>().ToList();
            if (test.Count == 0)
            {
                IService service = new T();
                activeServiceList.Add(service.Instance);
                availableServiceList.Remove(service.Instance.Name);
                return service;
            }
            else
                return test[0];
        }
        public IService LoadService(string serviceName)
        {
            bool test = activeServiceList.Exists(myService => myService.Name == serviceName);
            if (test == false)
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                Type[] types = assembly.GetTypes();
                IEnumerable<Type> imp = types.Where(t => t.GetInterfaces().Contains(instance.serviceType));
                foreach (Type type in imp)
                {
                    if (type.Name == serviceName)
                    {
                        IService service=(IService)Activator.CreateInstance(type);
                        activeServiceList.Add(service);
                        availableServiceList.Remove(serviceName);
                    }
                }
            }
            return null;
        }
        public void ServicesEventHandler(IService service)
        {

        }
        public void UnLoadService<T>() where T : IService
        {
            List<T> test = activeServiceList.OfType<T>().ToList();
            if (test.Count > 0)
            {
                int index = activeServiceList.IndexOf(test[0]);
                activeServiceList[index] = null;
                activeServiceList.RemoveAt(index);
                test[0].Kill();
                availableServiceList.Add(test[0].Instance.Name);
                test = null;
            }

        }

    }
    #region Interfaces
    public interface IService : INotify
    {
        IService Instance { get; }
        void Start();
        void Stop();
        void Pause();
        void Resume();
        void Kill();
        string ToJson();
        string Name{get;}
    }
    public interface INotify
    {
        void EventAdd(INotify notify);
        void EventRemove(INotify notify);
    }
    #endregion
    #region Services  
    public sealed class MySQLService : IService
    {
        private static MySQLService instance = null;

        public string server = "localhost";
        public string database = "dbtest";
        private string path = $"server={instance.server};database={instance.database};uid={instance.account.nickname};pwd={instance.account.Password}";
        private Account account;
        private MySqlConnection cnn;
        public static readonly string serviceName= "MySQL Connector";
        public IService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MySQLService();
                }
                return instance;
            }
        }
        public Account Account
        {
            get { return account; }
            set { account = value; }
        }
        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        public string Name
        {
            get
            {
              return  serviceName;
            }
        }

        public void EventAdd(INotify notify)
        {

        }

        public void EventRemove(INotify notify)
        {

        }

        public void Kill()
        {
            instance = null;
        }

        public void Pause()
        {

        }

        public void Resume()
        {
        }

        public void Start()
        {
            instance.cnn = new MySqlConnection(path);
            try
            {
                instance.cnn.Open();
                Console.WriteLine("Connection Open ! ");
            }
            catch (Exception e)
            {
                Console.WriteLine("Can not open connection ! " + e);
            }
        }

        public void Stop()
        {
            if (instance != null && cnn != null)
            {
                instance.cnn.Close();
            }
        }

        public string ToJson()
        {
            return null;
        }


    }
    #endregion
    public class Account
    {
        public string name;
        public string nickname;
        private string password;
        public string salt;
        public string Password
        {
            get
            {
                salt = CreateSalt(10);
                return GenerateSHA256Hash(password, salt); ;
            }
            set
            {
                password = value;
            }
        }

        private string GenerateSHA256Hash(string stringToHash, string salt)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(stringToHash + salt);
            System.Security.Cryptography.SHA256Managed sha256HashString = new System.Security.Cryptography.SHA256Managed();
            byte[] hashedString = sha256HashString.ComputeHash(bytes);
            return ByteArrayToString(hashedString);
        }
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
        public String CreateSalt(int size)
        {
            var rng = new System.Security.Cryptography.RNGCryptoServiceProvider();
            var buff = new byte[size];
            rng.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }
    }

}