﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CustomCollection
{
    class CollectionManager
    {
        public static void Main(string[] args)
        {
            Collection collection = new Collection(5);
            collection.ShowMenu();
        }
    }
    public class DynamicData
    {
        public delegate void DynamicDataHandler(DynamicData dynamicData);
        public static event DynamicDataHandler AddDynamicData;
        private string name;
        private string className;
        private object data; //J'ai fait le choix du type object pour pouvoir dynamiquement ajouter n'importe quels datas

        public DynamicData(string className,string name,object data)
        {
            this.className = className;
            this.name = name;
            this.data = data;
            AddDynamicData(this);
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string ClassName
        {
            get { return className; }
            set { className = value; }
        }
        public object Data
        {
            get { return data; }
            set { data = value; }
        }
    }
    public class Cell
    {
        private List<DynamicData> datas = new List<DynamicData>(); // Possibilité d'ajout de données ultérieurement

        public virtual void InitializeData()
        {
            AddData("Cell", "Zone de données", null);
            AddData("Cell", "Date", null);
        }
        public List<DynamicData> Datas
        {
            get
            {
                //Etant les datas par défaut, je les initialises ici si aucunes données n'existe dans datas
                if (datas.Count == 0)
                {
                    InitializeData();
                }
                return datas;
            }
        }

        public List<string> FieldsList
        {
            get
            {
                return Datas.Select(obj=>obj.Name).ToList(); 
            }
        }
        public void AddData(string className, string name, object data) //Méthode pour pouvoir ajouté une donnée plus tard
        {
            DynamicData dynamicData = new DynamicData(className,name,data);
            datas.Add(dynamicData);
        }
    }
    public class Client:Cell
    {
        public override void InitializeData()
        {
            AddData("Client", "Numéro de client", "");
            AddData("Client", "Nom", "");
            AddData("Client", "Prénom", "");
        }
        public Client()
        {
            InitializeData();
            bool isValidInput = false;
            Console.Clear();
            Console.WriteLine("Fonction d'ajout du client");
            Console.WriteLine("--------------------------\n");
            for (int i = 0; i < Datas.Count; i++)
            {
                while (!isValidInput)
                {
                    Console.WriteLine("Inserez la valeur du champs '" + Datas[i].Name + "' de type " + Datas[i].Data.GetType()+ ":");
                    string inputStream = Console.ReadLine();
                    try
                    {

                        if (inputStream == "")
                        {
                            throw new Exception("Donnée vide !");
                        }
                        //J'ai fait le choix d'un switch pour pouvoir faire la vérification de données plus facilement et suivant le type.
                        //J'ai limité les datas dynamique à 3 types (int, float, date). Au besoin, on pourra ajouté ici d'autre type.
                        switch (Datas[i].Data.GetType().ToString())
                        {
                            //N'étant pas demandé et ne générant pas d'erreur, je n'ai pas traité le cas numérique et caractère spéciaux dans une chaine de caractère.
                            case "System.String":
                                Datas[i].Data = inputStream;
                                break;
                            case "System.Int32":
                                Datas[i].Data = int.Parse(inputStream);
                                break;
                            case "System.Single":
                                Datas[i].Data = float.Parse(inputStream);
                                break;
                            case "System.DateTime":
                                Datas[i].Data = DateTime.Parse(inputStream);
                                break;
                            default:
                                throw new Exception("Type non géré -------> Voir switch class Client");

                        }
                        isValidInput = true;
                        if (i == Datas.Count)
                        {
                            Console.WriteLine("Client ajouté");
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.ReadLine();
                    }
                }
                isValidInput = false;
            }

        }      
        

    }
    public class Collection
    {
        private Cell[] collection;
        private List<DynamicData> listOfDynamicDatasFromCell = new List<DynamicData>();
        private List<DynamicData> listOfDynamicDatasFromClient = new List<DynamicData>();
        private List<string> clientDatasLabels = new List<string>();
        private List<string> cellDatasLabels = new List<string>();
        public Collection(int collectionSize)
        {
            DynamicData.AddDynamicData += AddDynamicData; //Delegate sur event qui perment de récupérer le DynamicData nouvellement créé
            collection = new Cell[collectionSize];
        }
        public void AddClient()
        {

            int index = SearchEmptyCell();

            if (index != -2)
            {
                Cell cell = new Cell();
                cell.Datas.Find(obj => obj.Name == "Date").Data = DateTime.Now;
                cell.Datas.Find(obj => obj.Name == "Zone de données").Data = new Client();
                collection[index] = cell;
                Console.WriteLine("Cellule ajoutée à la collection.\nAppuyer pour revenir au menu...");
            }
            else
            {
                Console.WriteLine("Plus de place dans la collection !\nAppuyer pour revenir au menu...");
                
            }
            Console.ReadLine();
            ShowMenu();
        }
        public void ShowMenu()
        {
            bool isValidInput = false;
            while (!isValidInput)
            {
                try
                {
                    Console.Clear();
                    Console.WriteLine("****************************************************");
                    Console.WriteLine("* Menu                                             *");
                    Console.WriteLine("*                                                  *");
                    Console.WriteLine("****************************************************");
                    Console.WriteLine("*    1. Ajouter un client                          *");
                    Console.WriteLine("*    2. Montrer la collection                      *");
                    Console.WriteLine("*    3. Quitter                                    *");
                    Console.WriteLine("****************************************************");
                    Console.WriteLine("Entrez votre choix:");
                    string inputStream = Console.ReadKey().KeyChar.ToString();
                    Console.ReadLine();
                    switch (int.Parse(inputStream))
                    {
                        case 1:
                            AddClient();
                            break;
                        case 2:
                            ShowCollection();
                            break;
                        case 3:
                            break;
                        default:
                            throw new Exception("Option inconnue !");

                    }
                    isValidInput = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }


        }
        public void ShowCollection()
        {
            int nbrChr = 0;
            string barre = "";
            Console.Clear();
            nbrChr = (cellDatasLabels.Count * 30) + 12 + cellDatasLabels.Count;
            AddXCharacter(ref barre,nbrChr,"*");
            Console.WriteLine("Liste de la collection");
            Console.WriteLine("----------------------\n");
            Console.WriteLine(barre);
            Console.WriteLine(ShowLabels(cellDatasLabels.ToArray()));
            Console.WriteLine(barre);
            Console.WriteLine(ShowDatas(collection.ToList(),cellDatasLabels.ToArray()));
            Console.WriteLine(barre);
            Console.WriteLine("Pour revenir au menu tapez 'm' ou tapez un numéro d'index :");

            bool isValidInput = false;
            while(!isValidInput)
            {
                
                string inputStream = Console.ReadLine();
                if (inputStream == "m")
                {
                    isValidInput = true;
                    ShowMenu();
                }
                else
                {
                    isValidInput = true;
                    try
                    {
                        int index = int.Parse(inputStream);
                        Client client;
                        if (collection[index]!=null)
                        {
                            
                            client = (Client)collection[index].Datas.Find(obj => obj.Name == "Zone de données").Data;
                            if (client != null)
                            {
                                ShowClient(client, index);

                            }
                            else
                            {
                                throw new Exception("Aucune donnée disponible !");
                            }
                        }
                        else
                        {
                            throw new Exception("Collection vide !");
                        }
                        
                        
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        isValidInput = false;
                    }
                    
                }
            }
            
            
        }
        public void ShowClient(Client client, int index)
        {
            int nbrChr = 0;
            nbrChr = (clientDatasLabels.Count * 30)+12+ clientDatasLabels.Count;
            string barre = "";
            AddXCharacter(ref barre,nbrChr,"*");
            Console.Clear();
            Console.WriteLine("Données");
            Console.WriteLine("-------\n");
            List<Cell> tempList = new List<Cell>();
            tempList.Add(client);
            Console.WriteLine(barre);
            Console.WriteLine(ShowLabels(clientDatasLabels.ToArray()));
            Console.WriteLine(barre);
            Console.WriteLine(ShowDatas(tempList,clientDatasLabels.ToArray()));
            Console.WriteLine(barre);
            Console.WriteLine("Appuyer pour revenir à la collection...");
            Console.ReadLine();
            ShowCollection();
        }
        private int SearchEmptyCell()
        {
            for (int i = 0; i < collection.Length; i++)
            {
                if (collection[i] == null)
                {
                    return i;
                }
            }
            return -2;
        }
       
        public string ShowDatas(List<Cell> datas,string [] fieldsLabels )
        {
            int nbrChr = 0;
            string line="";
            for (int i = 0; i < datas.Count; i++)
            {
                line += "*";
                nbrChr = (10 - i.ToString().Length) / 2;
                AddXCharacter(ref line, nbrChr);
                line += i;
                AddXCharacter(ref line, nbrChr);
                line += "*";
                for (int j = 0; j < fieldsLabels.Length; j++)
                {
                    
                    DynamicData data =
                        datas[i] != null  &&datas[i].Datas!=null ? datas[i].Datas.Find(obj => obj.Name == fieldsLabels[j]):null;
                    if (data != null && data.Data != null)
                    {
                        
                            nbrChr = (30 - data.Data.ToString().Length) / 2;
                            AddXCharacter(ref line, nbrChr);
                            line += data.Data;
                            AddXCharacter(ref line, nbrChr); 
                    }
                    else
                    {
                        nbrChr = 26 / 2;
                        AddXCharacter(ref line, nbrChr);
                        line += "null";
                        AddXCharacter(ref line, nbrChr);
                    }
                    line += "*";
                }
                if (i< datas.Count-1)
                {
                    line += "\n";
                }
                    
               
                
               
            }
            return line;
        }
        public string ShowLabels(string[] labels, int maxSize=0)
        {
            int nbrChr = 0;
            string line = "*  Index   *";
            for (int i = 0; i < labels.Length; i++)
            {
                nbrChr = (30 - labels[i].Length) / 2;
                AddXCharacter(ref line, nbrChr);
                line += labels[i];
                AddXCharacter(ref line, nbrChr);
                line += "*";
            }
            return line;
        }
    
        public void AddXCharacter(ref string str, int nbrSpace,string chr=" ")
        {
            for (int i = 0; i < nbrSpace; i++)
            {
                str += chr;
            }
        }
        private void AddDynamicData(DynamicData data)
        {
            if(data.ClassName== "Cell")
            {
                listOfDynamicDatasFromCell.Add(data);
                if (!cellDatasLabels.Contains(data.Name))
                {
                    cellDatasLabels.Add(data.Name);
                }
            }
            else if (data.ClassName == "Client")
            {
                
                listOfDynamicDatasFromClient.Add(data);
                if (!clientDatasLabels.Contains(data.Name))
                {
                    clientDatasLabels.Add(data.Name);
                }
            }
            
        }
    }

}
