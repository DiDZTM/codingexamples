﻿using System;

namespace QueueProject
{

    class Program
    {
        static void Main(string[] args)
        {
            //Initialisation de la Queue
            ActionQueue actionQueue=new ActionQueue(6);
            //Test de fonctionnement de la queue
            actionQueue.TestActionQueue();
        }
    }
    public class Action
    {
        private string nom;
        public int pion1X;
        public int pion1Y;
        public int pion2X;
        public int pion2Y;
        public string Nom
        {
            get { return nom; }
            set 
            {
                if (value.Length>20)
                {
                    Console.WriteLine("!! La chaîne contient plus de 20 caractères !!");
                }
                else
                {
                    nom = value;
                }
            }
        }
        public Action(string nom,int pion1X,int pion1Y,int pion2X,int pion2Y)
        {
            this.nom = nom;
            this.pion1X = pion1X;
            this.pion1Y = pion1Y;
            this.pion2X = pion2X;
            this.pion2Y = pion2Y;
        } 
    }
    public class ActionQueue
    {
        Action[] actionQueue;
        private int top;
        private int next;
        private int size;
        public ActionQueue(int size)
        {
            this.size = size;
            actionQueue = new Action[size];
            top = 0;
            next = 0;
        }
        public void PrintQueue()
        {
            Console.WriteLine("********** next :"+ next+" *************");
            for (int i = 0; i < actionQueue.Length; i++)
            {
                Console.WriteLine("|"+" Index |"+ i + "| Action | " + ((actionQueue[i]==null)?"null   " :actionQueue[i].Nom)+" |");
            }
            Console.WriteLine("*******************************");
        }
      
        public void Add(Action action) 
        {
            for (int i = 0; i < actionQueue.Length; i++)
            {
                if (i > next && actionQueue[i] == null && next != top)//Gestion switch queue
                {
                    Action actionTemp = actionQueue[next];
                    actionQueue[i] = actionTemp;
                    actionQueue[next] = action;
                    next++;
                    Console.WriteLine("-> Action correctement ajoutée dans la queue : " + action.Nom + " Index :" + i);
                    break;
                }
                else if (actionQueue[i] == null)
                {
                    actionQueue[i] = action;
                    Console.WriteLine("-> Action correctement ajoutée dans la queue : " + action.Nom + " Index :" + i);
                    break;
                }
                if (actionQueue[i] != null && actionQueue.Length - 1 == i)
                {
                    Console.WriteLine("!! Plus place dans la queue !! : " + action.Nom);
                }
            }
        }
        //J'ai supposé que la fonction Get récupère l'action donc libère l'espace dans la queue
        //car aucune méthode ExecuteAction ne devait être implémentée donc l'espace ne pouvait se libérer.
        public Action Get()
        {
           
            Action action = actionQueue[next];
            if (action != null)
            {
                Console.WriteLine("-> Action trouvée dans la queue : "+ action.Nom+ " Index :"+next);
                
                actionQueue[next] = null;
                //gestion du switch queue
                if (next + 1 < actionQueue.Length && actionQueue[next + 1] == null) //Switch index
                {
                    int nextTemp = next;
                    next = top;
                }
                else if (next + 1 < actionQueue.Length && actionQueue[next + 1] != null)//Normal
                {
                    next++;
                }
                else //End of queue
                {
                    next = top;
                }
                
            }
            else
            {
                Console.WriteLine("-> Pas d'action dans la queue ");
            }
            return action; 
        }
       
        //Effectue une routine pour tester ActionQueue. 
        //Attention il faut que la queue ne contienne rien car ceci réinitialise tout
        public void TestActionQueue( )
        {
            actionQueue = new Action[6];
            Add(new Action("action 01", 1, 1, 1, 1));
            Add(new Action("action 02", 2, 2, 2, 2));
            Add(new Action("action 03", 3, 3, 3, 3));
            Add(new Action("action 04", 4, 4, 4, 4));
            Add(new Action("action 05", 5, 5, 5, 5));
            Add(new Action("action 06", 6, 6, 6, 6));
            Add(new Action("action 07", 7, 7, 7, 7));
            PrintQueue();
            Action testAction=Get();
            //Console.WriteLine("-DEBUG- testAction.Name  ----> "+testAction.Nom );
            Get();
            PrintQueue();
            Add(new Action("action 08", 8, 8, 8, 8));
            PrintQueue();
            Get();
            PrintQueue();
            Get();
            PrintQueue();
            Add(new Action("action 09", 9, 9, 9, 9));
            PrintQueue();
            Get();
            PrintQueue();
            Get();
            PrintQueue();
            Get();
            PrintQueue();
            Add(new Action("action 10", 10, 10, 10, 10));
            PrintQueue();
            Add(new Action("action 11", 11, 11, 11, 11));
            PrintQueue();
            Add(new Action("action 12", 11, 11, 11, 11));
            PrintQueue();
            Add(new Action("action 13", 11, 11, 11, 11));
            PrintQueue();
            Get();
            PrintQueue();
            Get();
            PrintQueue();
            Get();
            PrintQueue();
            Get();
            PrintQueue();
            Get();
            PrintQueue();
            Console.ReadLine();
        }
    }
    
}
