﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game(10, 10);
            Console.ReadLine();

        }
    }
    public enum ENumberToLetter { A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z}
    public class Game
    {
        private Dictionary<int, Ship> p1ShipList = new Dictionary<int, Ship>();
        private Dictionary<int, Ship> p2ShipList = new Dictionary<int, Ship>();
        //State -2=nothing -3=hit other shipId 
        private int[,] p1Board;
        private int[,] p2Board;
  
        public Game(int x, int y)
        {
            p1ShipList = InitialiseShipList(p1ShipList);
            p2ShipList = p1ShipList;
            p1Board = InitBoard(x, y);
            p2Board = p1Board;
            StartInsertionShip(p1Board, p1ShipList);
        }
        private Dictionary<int, Ship> InitialiseShipList(Dictionary<int, Ship> ships)
        {
            Ship ship;
            int id = 1;
            for (int i = 0; i < 4; i++)
            {
                ship = new Ship("Sous-marin", 1, id);
                ships.Add(id, ship);
                id++;
            }
            for (int i = 0; i < 3; i++)
            {
                ship = new Ship("Torpilleur", 2,id);
                ships.Add(id, ship);
                id++;
            }
            for (int i = 0; i < 2; i++)
            {
                ship = new Ship("Croiseur", 3,id);
                ships.Add(id, ship);
                id++;
            }
            ship = new Ship("Porte-avion", 4,id);
            ships.Add(id, ship);

            return ships;

        }
        private int[,] InitBoard(int x, int y)
        {
            int[,] board = new int[x, y];
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    board[i, j] = -2;
                }
            }
            return board;
        }
        private string ShipOrientationPossibilities(int[,] playerBoard, int x,int y, Ship ship)
        {
            string orientation = "";
            bool isUp = true;
            bool isDown = true;
            bool isLeft = true;
            bool isRight = true;
            if (ship.Size!=1)
            {
                for (int i = 0; i < ship.Size; i++)
                {

                    if (!(x + 1 - ship.Size < 0))
                    {
                        if (playerBoard[x - i, y] != -2 && isUp)
                        {
                            isUp = false;
                        }
                    }
                    else isUp = false;

                    if (!(x - 1 + ship.Size > playerBoard.GetLength(0)))
                    {
                        if (playerBoard[x + i, y] != -2 && isDown)
                        {
                            isDown = false;
                        }
                    }
                    else isDown = false;

                    if (!(y + 1 - ship.Size < 0))
                    {
                        if (playerBoard[x, y - i] != -2 && isLeft)
                        {
                            isLeft = false;
                        }
                    }
                    else isLeft = false;

                    if (!(y - 1 + ship.Size > playerBoard.GetLength(1)))
                    {
                        if (playerBoard[x, y + i] != -2 && isRight)
                        {
                            isRight = false;
                        }
                    }
                    else isRight = false;
                }

                if (isUp) orientation += "(H)aut ";
                if (isDown) orientation += "(B)as ";
                if (isLeft) orientation += "(G)auche ";
                if (isRight) orientation += "(D)roite ";

            }
            else if (playerBoard[x, y] != -2)
            {
                orientation = "EXIST";
            }
            return orientation;

        }
        public void ShipPlacement(int[,] board, Ship ship, Char orientation,int x,int y)
        {
            if (ship.Size == 1)
            {
                board[x,y] = ship.Id;
            }
            else
            {
                for (int i = 0; i < ship.Size; i++)
                {
                    if (orientation == 'B')
                    {
                        board[x + i, y] = ship.Id;
                    }
                    if (orientation == 'H')
                    {
                        board[x - i, y] = ship.Id;
                    }
                    if (orientation == 'G')
                    {
                        board[x, y - i] = ship.Id;
                    }
                    if (orientation == 'D')
                    {
                        board[x, y + i] = ship.Id;
                    }
                }
            }
            
        }
        public string ShowBoard(int[,] board, Dictionary<int, Ship> playerShips, bool isPlacing= false)
        {
            string boardShow = "";
            int boardValue;
            for (int i = 0; i < board.GetLength(0); i++)
            {
                boardShow += "   ";
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    boardValue = board[i, j];
                    if (boardValue < 0)
                    {
                        if (boardValue == -2)
                        {
                            boardShow += "  |";
                        }
                        else
                        {
                            boardShow += "  X";
                        } 
                    }
                    else 
                    {
                        if (isPlacing)
                        {
                            boardShow += " " + playerShips[board[i, j]].Name[0] + "|";
                        }
                        
                    }
                    
                }
                boardShow += "\n";
            }
            return boardShow;
        }
        public void ShowGame()
        {
            Console.WriteLine("Bataille navale");
            Console.WriteLine("---------------\n");

        }
        public void StartInsertionShip(int[,] board, Dictionary<int, Ship> playerShips)
        {
            Ship ship;
            int totalShips;
            int shipCounter = 1;
            string currentShip="";
            for (int i = 0; i < playerShips.Count; i++)
            {
                ship = playerShips.ElementAt(i).Value;

                if (currentShip != ship.Name) shipCounter = 1;

                currentShip = ship.Name;
                totalShips = p1ShipList.Values.Where(obj => obj.Name == currentShip).Count();
                Console.WriteLine(
                    "Insertion du " + currentShip + 
                    "(taille:" +ship.Size+" nbr:"+shipCounter+"/"+ totalShips + 
                    ") sur la grille. Pièces restantes:"+ (playerShips.Count-i) + "/"+ playerShips.Count+"\n");
                Console.WriteLine("Coordonnée X:");
                int x = int.Parse(Console.ReadLine());
                Console.WriteLine("Coordonnée Y:");
                int y = int.Parse(Console.ReadLine());
              
                
                string orientationPossibilities = ShipOrientationPossibilities(p1Board, x, y, ship);
                if (orientationPossibilities=="EXIST")
                {
                    Console.WriteLine("Emplacement pris...");
                    i--;
                    continue ;
                }
                char orientation = '\0';
                if (orientationPossibilities!="")
                {
                    Console.WriteLine("Orientation " + orientationPossibilities + ":");
                    orientation = Console.ReadLine().ToString().ToUpper()[0];
                }
               
                ShipPlacement(p1Board,ship,orientation,x,y);
                Console.WriteLine( ShowBoard(p1Board,p1ShipList,true));
                shipCounter++;

            }
        }
    }
    public class Ship
    {
        private string name;
        private int life;
        private int size;
        private int posX;
        private int posY;
        private int id;
        public Ship(string name, int size,int id=0)
        {
            this.name = name;
            this.size = size;
            this.life = size;
            this.id = id;

        }
      
        public int PosY
        {
            get { return posY; }
            set { posY = value; }
        }


        public int PosX
        {
            get { return posX; }
            set { posX = value; }
        }


        public int Life
        {
            get { return life; }
            set { life = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Size
        {
            get { return size; }
            set { size = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
    }
}
